var config = {
	target: "web",
    debug: true,
    devtool: "source-map",
    entry: {
        main: "./src/main.jsx"
    },
    output: {
        path: "./build",
        filename: "[name].bundle.js"
    },
    resolve: {
        modulesDirectories: ['bower_components', 'node_modules'],
    },
    module: {
        loaders: [
					{ test: /\.css/, loader: "style-loader!css-loader" },
					{ test: /\.less$/, loader: "style-loader!css-loader!less-loader" },
					{ test: /\.jsx$/, loader: "jsx-loader?harmony" },
					{ test: /\.png/, loader: "url-loader?limit=100000&mimetype=image/png" },
					{ test: /\.gif/, loader: "url-loader?limit=100000&mimetype=image/gif" },
					{ test: /\.jpg/, loader: "file-loader"} ,
					{ test: /\.svg/, loader: "url-loader?limit=10000"},
					{ test: /\.woff/, loader: "url-loader?limit=100000"},
					{ test: /\.woff2/, loader: "url-loader?limit=100000"},
					{ test: /\.ttf/, loader: "file-loader"},
					{ test: /\.eot/, loader: "file-loader"}
        ]
    }
};

module.exports = config;
