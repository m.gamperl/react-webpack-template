var injectTapEventPlugin = require("react-tap-event-plugin");

//Needed for onTouchTap
//Can go away when react 1.0 release
//Check this repo:
//https://github.com/zilverline/react-tap-event-plugin
injectTapEventPlugin();


var React  = require('react');
var Router = require('react-router');
var routes = require('./app/routes.jsx');

require("font-awesome/less/font-awesome.less");
require("react-bootstrap");
require('./styles/main.less');


//require("bootstrap");
require("react-bootstrap");

document.addEventListener("DOMContentLoaded", function(event) {

  React.initializeTouchEvents(true);

  Router.run(routes, Router.HashLocation, function (Handler, state) {
    React.render(<Handler />, document.getElementById('content'));
  });

});
