"use strict";

var React         = require('react');
var Router        = require('react-router');
var Route         = Router.Route;
var DefaultRoute  = Router.DefaultRoute;
var NotFoundRoute = Router.NotFoundRoute;

/* Components */
var App      = require('./app.jsx');
var Index    = require("./index.jsx");

var routes = (
  <Route name="places" path="/" handler={App}>
    <DefaultRoute name="index" pth="/index" handler={Index} />
    <NotFoundRoute name="notfound" handler={ NotFoundRoute }/>
  </Route>
);

module.exports = routes;
